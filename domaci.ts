﻿interface ImaPovrsinu{    
    izracunajPovrsinu():number;
    prikaziPodatke(): string;
}

class GeomOblik{
    private _ime:string;
    constructor(ime:string){
        this._ime=ime;
    }
    
    getIme():string{
        return this._ime;
    }
    setIme(ime:string){
        this._ime = ime; 
    }
}

class Krug extends GeomOblik implements ImaPovrsinu{
    private _poluprecnik:number
    constructor(poluprecnik:number){
        super("Krug");
        this._poluprecnik = poluprecnik;
    }

    getPoluprecnik():number{
        return this._poluprecnik;
    }

    setPoluprecnik(poluprecnik:number){
        this._poluprecnik = poluprecnik;
    }

    izracunajPovrsinu():number{
        return this._poluprecnik * this._poluprecnik * Math.PI;
    }

    prikaziPodatke(): string{
        return "Za oblik " + this.getIme() 
            + " sa poluprecnikom " + this.getPoluprecnik() 
            + " povrsina je " + this.izracunajPovrsinu();
    }
}

class Kvadrat extends GeomOblik implements ImaPovrsinu{
    private _stranica: number;
    
    constructor(stranica:number, ime:string = "Kvadrat"){
        super(ime);
        this._stranica = stranica;
    }

    getStranica():number{
        return this._stranica;
    }
    setStranica(stranica:number){
        this._stranica = stranica;
    }

    izracunajPovrsinu(): number {
        return this._stranica * this._stranica;
    }

    prikaziPodatke(): string{
        return "";
    }
}

class Pravougaonik extends GeomOblik implements ImaPovrsinu{
    private _sirina:number;
    private _visina:number;
    
    constructor(sirina:number, visina:number, ime:string = "Pravougaonik"){
        super(ime);
        this._sirina = sirina;
        this._visina = visina;
    }

    getVisina():number{
        return this._visina;
    }
    setVisina(visina: number){
        this._visina = visina;
    }

    getSirina():number{
        return this._sirina;
    }
    setSirina(sirina: number){
        this._sirina = sirina;
    }

    izracunajPovrsinu(): number {
        return this._sirina * this._visina;
    }

    prikaziPodatke(): string{
        return "Za oblik " + this.getIme() 
            + " sa visinom " + this.getVisina()
            + " i sirinom " + this.getSirina()
            + " povrsina je " + this.izracunajPovrsinu();
    }
}


interface ImaZapreminu{    
    izracunajZapreminu():number;
    prikaziPodatke(): string;
}

class Kocka extends Kvadrat implements ImaZapreminu {
    constructor(stranica:number){
        super(stranica, "Kocka");
        this.setStranica(stranica);
    }
    
    izracunajPovrsinu(): number{
        return 6 * super.izracunajPovrsinu();
    }

    izracunajZapreminu(): number {
        var s = this.getStranica();
        return s * s * s;
    }

    prikaziPodatke(): string {        
        return "Za geometrijsko telo " + this.getIme()
            + " sa stranicom " + this.getStranica()
            + " povrsina je " + this.izracunajPovrsinu()
            + " zapremina je " + this.izracunajZapreminu();
    }
}

class Kvadar extends Pravougaonik implements ImaZapreminu{
    sirina: any;
    private _duzina: number; 
    
    constructor(visina:number, sirina:number,duzina:number){
        super(visina, sirina, "Kvadar");
        this._duzina=duzina;
    }
    getDuzina():number{
        return this._duzina;
    }
    setDuzina(duzina:number){
        this._duzina = duzina; 
    }
    izracunajPovrsinu(): number{
        return 2 * (
            this._duzina * this.getSirina()
            + this._duzina * this.getVisina()
            + this.getSirina() * this.getVisina());
    }

    izracunajZapreminu(): number {     
        return this._duzina * this.getSirina() * this.getVisina();
    }

    prikaziPodatke(): string {        
        return "Za geometrijsko telo " + this.getIme()
            + " sa duzinom " + this.getDuzina()
            + " i sirinom " + this.getSirina()
            + " i visinom " + this.getVisina()
            + " povrsina je " + this.izracunajPovrsinu()
            + " zapremina je " + this.izracunajZapreminu();
    }
}

class Test{
    public static main(){
        var oblici = [new Krug(1), new Kocka(5), new Kvadar(3,4,5)];

        oblici.forEach(element => {
            console.log(element.prikaziPodatke());
        });
    }
}

Test.main();